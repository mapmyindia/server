<?php

function findUser($where, $Model_users){
	$user = $Model_users->get_by($where);
	if(isset($user['id'])){
		return $user;
	}
	return false;
}

function deleteUser($UserId, $context){

	$context->load->model('Model_students');
	$student = $context->Model_students->get_by(array('UserID'=> $UserId)); 
	if (isset($student['id'])) {	
		if ($student['Special'] == normal) {
			$context->Model_students->delete($student['id']);
			return true;
		}else{
			return false;
		}
	}

	$context->load->model('Model_faculty');
	$faculty = $context->Model_faculty->get_by(array('UserID'=> $UserId)); 
	if (isset($faculty['id'])) {	
		if ($faculty['Special'] == normal) {
			$context->Model_faculty->delete($faculty['id']);
			return true;
		}else{
			return false;
		} 
	}

	$context->load->model('Model_warden');
	$warden = $context->Model_warden->get_by(array('UserID'=> $UserId)); 
	if (isset($warden['id'])) {	
		if ($warden['Special'] == normal) {
			$context->Model_warden->delete($warden['id']);
			return true;
		}else{
			return false;
		} 
	}

	$context->load->model('Model_staff');
	$staff = $context->Model_staff->get_by(array('UserID'=> $UserId)); 
	if (isset($staff['id'])) {	
		if ($staff['Special'] == normal) {
			$context->Model_staff->delete($staff['id']);
			return true;
		}else{
			return false;
		} 
	}

	return false;
}

?>