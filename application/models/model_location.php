<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_location extends MY_Model{
	protected $_table = 'locations';
	protected $primary_key = 'id';
	protected $return_type = 'array';
}

?>