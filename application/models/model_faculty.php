<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_faculty extends MY_Model{
	protected $_table = 'faculty';
	protected $primary_key = 'id';
	protected $return_type = 'array';
}

?>