<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_comments extends MY_Model{
	protected $_table = 'comments';
	protected $primary_key = 'id';
	protected $return_type = 'array';
}

?>