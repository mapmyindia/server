<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_students extends MY_Model{
	protected $_table = 'students';
	protected $primary_key = 'id';
	protected $return_type = 'array';
}

?>