<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';


class Temp extends REST_Controller{

	function __construct()
	{
		parent::__construct();
	}

	function image_post()
	{
		// Path to move uploaded files
		$target_path = "./uploads/";	 
		
		$user_full=$this->session->all_userdata();
		
	 	if(!isset($user_full['user_info'])){
	 		$this->response(array('success'=>false, 'message'=>'session expired'));
	 	}else{
	 		if (isset($_FILES['image']['name'])) {

				ini_set('upload_max_filesize', '10M');
				ini_set('post_max_size', '10M');
				ini_set('max_input_time', 300);
				ini_set('max_execution_time', 300);

			    $target_path = $target_path . basename($_FILES['image']['name']);
			    try {
			        // Throws exception incase file is not being moved
			        if (!move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {
			            // make error flag true
			            $this->response(array('success'=>false, 'message'=>'Could not move the file!'));
			        }else{
			        	//File successfully uploaded
			        	$data = $this->post();
			        	$data['id']=null;
			        	$data['videoPath']=$target_path;
			        	$data['userId']=$user_full['user_info']['username'];

			        	$this->load->model('Model_video');
			        	if ($this->Model_video->insert($data, false)!=false) {
							$this->response(array('success'=>true, 'message'=>'File uploaded successfully!'));
						}else{
							$this->response(array('success'=>false, 'message'=>'couldn\'t update database'));
						}
			        }
			    } catch (Exception $e) {
			        // Exception occurred. Make error flag true
			        $this->response(array('success'=>false, 'message'=>$e->getMessage()));
			    }
			} else {
			    // File parameter is missing
			    $this->response(array('success'=>false, 'message'=>'Not received any file!'));
			}
	 	}
	}
}

?>