<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Home extends REST_Controller{
	
	function __construct() {
		parent::__construct();
	}

	function login_get(){

		$data = $this->get();

		$this->load->helper('find_user');
		$user = findUser(array('UserID'=> $data['UserId']), $this);
		if (isset($user['error'])) {
			switch ($user['error']) {
				case error_none:
					if (isset($user['info'])&&isset($user['info']['Password'])) {
						if ($user['info']['Password'] == $data['Password']) {
							$this->session->set_userdata('user_info', $user);
							$this->response(array('Message'=> '', 'success'=> true));
						}else{
							$this->response(array('Message'=> 'Incorrect Password', 'success'=> false));
						}
					}else{
						$this->response(array('Message'=> 'Password field must be set.', 'success'=> false));
					}
					break;

				case error_username:
					$this -> response(array('Message'=>'No such ID exists', 'success'=> false));
					break;
				
				default:
					$this -> response(array('Message'=>'Invalid user error code.', 'success'=> false));
					break;
			}
		}else{
			$this->response(array('Message'=> 'user error not set', 'success'=> false));
		}
	}

	function logout_get(){
		$this->session->sess_destroy();
		$this->response(array('success' => true));
	}
}

?>