<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Location extends REST_Controller{

	function __construct() {
		parent::__construct();
	}

	/**
	*add location to the database
	*also add user information
	*/
	function add_post(){

		$data = $this->post();
		$this->load->model('Model_location');
		$data['id'] = null;
		//retrieve user data
		$user_full = $this->session->all_userdata();

		$user = $user_full['user_info'];

		//check if user session/user information present
		if(!isset($user['username'])){
			$this->response(array('success'=>false, 'message'=>'Session Expired/User not registered'));
		}else{
			$data['userId'] = $user['username'];
			//check if insert is successful 
			if ($this->Model_location->insert($data, false)!=false) {
				$this->response(array('success'=>true, 'message'=>''));
			}else{
				$this->response(array('success'=>false, 'message'=>'Something went wrong:P'));
			}
		}
	}
}

?>