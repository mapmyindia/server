<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Institute extends REST_Controller{
	function __construct() {
		parent::__construct();
	}

	function complain_get(){
		$data = $this->get();
		$this->load->model('Model_institute');
		$sess_user = $this->session->all_userdata();
		if (isset($sess_user['user_info'])) {
			$user = $sess_user['user_info']['info'];
			$comp['id'] = null;
			$comp['SenderID'] = $user['UserID'];
			$comp['Resolved'] = unresolved;
			$comp['Title'] = $data['Title'];
			$comp['Content'] = $data['Content'];
			$comp['UpIDs'] = json_encode(array('ids'=>[]));
			$comp['DownIDs'] = json_encode(array('ids'=>[]));
			$comp['Upvotes'] = zero;
			$comp['Downvotes'] = zero;
			$comp['CreatedTime'] = date('Y-m-d H:i:s');
			$comp['UpdatedTime'] = $comp['CreatedTime'];
			$comp['CommentIDs'] = json_encode(array('ids'=>[]));
			$result = $this->Model_institute->insert($comp, false);
			if ($result == false) {
				$this->response(array('success'=>false, 'Message'=> 'Validation Error'));
			}else{
				$this->response(array('success'=> true, 'Message'=> ''));
			}
		}else{
			$this->response(array('success'=>false, 'Message' => 'User session expired.'));
		}
	}

    

	function getcomplaint_get(){
		$data = $this->get();
		$this->load->model('Model_institute');
		$row = $this->Model_institute->get_by(array('id'=> $data['id']));
		$this->load->helper('find_user');
		$final_result = array();
		$find_user = findUser(array('UserID'=> $row['SenderID']), $this);
		if (isset($find_user['info'])) {
			$final_result['FirstName'] = $find_user['info']['FirstName'];
			$final_result['LastName'] = $find_user['info']['LastName'];
		}
		else{
			$final_result['FirstName']='Unknown';
			$final_result['LastName']='User';
		}
		$final_result['Resolved'] = $row['Resolved'];
		$final_result['Title'] = $row['Title'];
		$final_result['Content'] = $row['Content'];
		$final_result['Upvotes'] = $row['Upvotes'];
		$final_result['Downvotes'] = $row['Downvotes'];
		$sess_user = $this->session->all_userdata();
		if (isset($sess_user['user_info'])) {
			$UserId = $sess_user['user_info']['info']['UserID'];
			$upvoted_ids = json_decode($row['UpIDs'], true);
			$downvoted_ids = json_decode($row['DownIDs'], true);
			if (in_array($UserId, $upvoted_ids['ids'])) {
				$final_result['Voted'] = upvote;
			}else if (in_array($UserId, $downvoted_ids['ids'])) {
				$final_result['Voted'] = downvote;
			}else{
				$final_result['Voted'] = novote;
			}
		}else{
			$this->response(array('success'=>false, 'Message'=>'User session expired', 'complaint'=> ''));
		}
		$final_result['CreatedTime'] = $row['CreatedTime'];
		$final_result['UpdatedTime'] = $row['UpdatedTime'];
		$comments = [];
		if (isset($row['CommentIDs'])) {
			$this->load->model('Model_comments');
			$comment_ids = json_decode($row['CommentIDs'], true);
			foreach ($comment_ids['ids'] as $id) {
				$each_comp = $this->Model_comments->get_by(array('id'=> $id));
				if (isset($each_comp['id'])) {
					$final_comp['UserId'] = $each_comp['UserID'];
					$find_user = findUser(array('UserID'=> $each_comp['UserID']), $this);
					if (isset($find_user['info'])) {
						$final_comp['FirstName'] = $find_user['info']['FirstName'];
						$final_comp['LastName'] = $find_user['info']['LastName'];
					}
					else{
						$final_comp['FirstName'] = 'Unknown';
						$final_comp['LastName']  = 'User';
					}
					$final_comp['Content'] = $each_comp['Content'];
					$final_comp['CreatedTime'] = $each_comp['CreatedTime'];
					array_push($comments, $final_comp);
				}
			}
			$final_result['comments'] = $comments;
			$this->response(array('success'=>true, 'Message'=>'', 'complaint'=> $final_result));
		}else{
			$this->response(array('success'=>false, 'Message'=> 'User ID not found.', 'complaint'=> ''));
		}
	}

	function resolve_get(){
		$data = $this->get();
    	$user = $this->session->all_userdata();
    	$this->load->model('Model_institute');
    	$this->load->model('Model_staff');
    	$row = $this->Model_institute->get_by(array('id'=>$data['id']));
    	$diro =$this->Model_staff->get_by(array('Type'=>'director'));
    	if(isset($row['id'])){
    		if (isset($diro['id'])) {
    			if($diro['UserID'] == $user['user_info']['info']['UserID']){
	                $final_comp['Resolved']= resolved;
	                $final_comp['UpdatedTime']= date('Y-m-d H:i:s');
	                $this->Model_institute->update($data['id'],$final_comp,false);
	                $this->response(array('success'=>true,'message'=>''));
	    		}
	    		else{
	    			$this->response(array('success'=>false,'message'=>'You are not authorized'));
	    		}
    		}else{
    			$this->response(array('success'=>false,'message'=>'Director unavailable'));
    		}
    	}
    	else{
    		$this->response(array('success'=>false,'message'=>'Complaint does not exist with this ID'));
    	}
	}

	function vote_get(){
		$data = $this->get();
		$voted = novote;
		$this->load->model('Model_institute');
		$row = $this->Model_institute->get_by(array('id'=> $data['id']));
		$final_result = array();
		$sess_user = $this->session->all_userdata();
		if (isset($sess_user['user_info'])) {
			$UserId = $sess_user['user_info']['info']['UserID'];
			$upvoted_ids = json_decode($row['UpIDs'], true);
			$downvoted_ids = json_decode($row['DownIDs'], true);
			if (in_array($UserId, $upvoted_ids['ids'])) {
				$voted = upvote;
			}else if (in_array($UserId, $downvoted_ids['ids'])) {
				$voted = downvote;
			}else{
				$voted = novote;
			}
			switch ($voted) {
				case novote:
					if ($data['upvote'] == 1) {
						array_push($upvoted_ids['ids'], $UserId);
						$final_result['UpIDs'] = json_encode($upvoted_ids, true);
						$final_result['Upvotes'] = $row['Upvotes'] + 1;
						$final_result['Downvotes'] = $row['Downvotes'];
					}else{
						array_push($downvoted_ids['ids'], $UserId);
						$final_result['DownIDs'] = json_encode($downvoted_ids, true);
						$final_result['Downvotes'] = $row['Downvotes'] + 1;
						$final_result['Upvotes'] = $row['Upvotes'];
					}
					break;

				case upvote:
					if ($data['upvote'] == 1) {
						$final_result['Downvotes'] = $row['Downvotes']+0;
						$final_result['Upvotes'] = $row['Upvotes']+0 ;
						$this->response(array('success'=>false, 'Message'=>'upvoted already', 'Upvotes'=> $final_result['Upvotes'], 'Downvotes'=> $final_result['Downvotes']));
						return;
					}else{
						array_push($downvoted_ids['ids'], $UserId);
						$final_result['DownIDs'] = json_encode($downvoted_ids, true);
						$upvoted_ids['ids'] = $this->remove_array($upvoted_ids['ids'], $UserId);
						$final_result['UpIDs'] = json_encode($upvoted_ids, true);
						$final_result['Downvotes'] = $row['Downvotes'] + 1;
						$final_result['Upvotes'] = $row['Upvotes'] - 1;
					}
					break;

				case downvote:
					if ($data['upvote'] == 2) {
						$final_result['Downvotes'] = $row['Downvotes']+0;
						$final_result['Upvotes'] = $row['Upvotes']+0 ;
						$this->response(array('success'=>false, 'Message'=>'downvoted already', 'Upvotes'=> $final_result['Upvotes'], 'Downvotes'=> $final_result['Downvotes']));
						return;
					}else{
						array_push($upvoted_ids['ids'], $UserId);
						$final_result['UpIDs'] = json_encode($upvoted_ids, true);
						$downvoted_ids['ids'] = $this->remove_array($downvoted_ids['ids'], $UserId);
						$final_result['DownIDs'] = json_encode($downvoted_ids, true);
						$final_result['Downvotes'] = $row['Downvotes'] - 1;
						$final_result['Upvotes'] = $row['Upvotes'] + 1;
					}
					break;
				
				default:
					$this->response(array('success'=>false, 'Message'=>'Default case', 'Upvotes'=> $row['Upvotes'], 'Downvotes'=> $row['Downvotes']));
					return;
					break;
			}
			$final_result['UpdatedTime']=date('Y-m-d H:i:s');
			$result = $this->Model_institute->update($data['id'], $final_result, false);
			if ($result == false) {
				$final_result['Downvotes'] = $row['Downvotes']+0;
						$final_result['Upvotes'] = $row['Upvotes']+0 ;
						$this->response(array('success'=>false, 'Message'=>'could not update', 'Upvotes'=> $final_result['Upvotes'], 'Downvotes'=> $final_result['Downvotes']));return;
			}
			$this->response(array('success'=>true, 'Message'=>'', 'Upvotes'=> $final_result['Upvotes'], 'Downvotes'=> $final_result['Downvotes']));
		}else{
			$this->response(array('success'=>false, 'Message'=>'User session expired', 'complaint'=> ''));
		}
	}

	function remove_array($array, $data){
		$result = array();
		foreach ($array as $value) {
			if($data!=$value){
				array_push($result,$value);
			}
		}
		return $result;
	}
	 function pin_get(){
    	$data = $this->get();
    	$this->load->helper('find_user');
    	$this->load->model('Model_institute');
    	$row = $this->Model_institute->get_by(array('id'=>$data['id']));
    	$sess_user=$this->session->all_userdata();
    	if(isset($row['id'])  ){
    	
    	switch ($sess_user['user_info']['type']) {
    		case '1':
    			$this->load->model('Model_students');
    			$user = $this->Model_students->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['InstitutePins'], true);
    			array_push($pinIds['pins'], $data['id']);
    			$final_result['InstitutePins'] = json_encode($pinIds, true);
    			$this->Model_students->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'InstitutePins'=>$final_result['InstitutePins']));
    			break;
    		case '2':
    			$this->load->model('Model_faculty');
    			$user = $this->Model_faculty->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['InstitutePins'], true);
    			array_push($pinIds['pins'], $data['id']);
    			$final_result['InstitutePins'] = json_encode($pinIds, true);
    			$this->Model_faculty->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'InstitutePins'=>$final_result['InstitutePins']));
    			break;
    		case '3':
    			$this->load->model('Model_warden');
    			$user = $this->Model_warden->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['InstitutePins'], true);
    			array_push($pinIds['pins'], $data['id']);
    			$final_result['InstitutePins'] = json_encode($pinIds, true);
    			$this->Model_warden->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'InstitutePins'=>$final_result['InstitutePins']));
    			break;
    		case '4':
    			$this->load->model('Model_staff');
    			$user = $this->Model_staff->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['InstitutePins'], true);
    			array_push($pinIds['pins'], $data['id']);
    			$final_result['InstitutePins'] = json_encode($pinIds, true);
    			$this->Model_staff->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'InstitutePins'=>$final_result['InstitutePins']));
    			break;		
    		default:
    			$this->response(array('success'=>false,'Message'=>'no ID exists with this'));
    			
    			break;
    	}
    	}else{
    		$this->response(array('success'=>false,'Message'=>'No such complaint exists with given ID or Not your Complaint'));
    	}

    }

    function unpin_get(){
    	$data = $this->get();
    	$this->load->helper('find_user');
    	$this->load->model('Model_institute');
    	$row = $this->Model_institute->get_by(array('id'=>$data['id']));
    	$sess_user=$this->session->all_userdata();
    	if(isset($row['id'])  ){
    	
    	switch ($sess_user['user_info']['type']) {
    		case '1':
    			$this->load->model('Model_students');
    			$user = $this->Model_students->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['InstitutePins'], true);
    			if(in_array($data['id'], $pinIds['pins'])) {
                    $pinIds['pins'] = $this->remove_array($pinIds['pins'], $data['id']);
    			}
    			else{
    			array_push($pinIds['pins'], $data['id']);
    		    }
    			$final_result['InstitutePins'] = json_encode($pinIds, true);
    			$this->Model_students->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'InstitutePins'=>$final_result['InstitutePins']));
    			break;
    		case '2':
    			$this->load->model('Model_faculty');
    			$user = $this->Model_faculty->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['InstitutePins'], true);
    			if(in_array($data['id'], $pinIds['pins'])) {
                    $pinIds['pins'] = $this->remove_array($pinIds['pins'], $data['id']);
    			}
    			else{
    			array_push($pinIds['pins'], $data['id']);
    		    }
    			$final_result['InstitutePins'] = json_encode($pinIds, true);
    			$this->Model_faculty->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'InstitutePins'=>$final_result['InstitutePins']));
    			break;
    		case '3':
    			$this->load->model('Model_warden');
    			$user = $this->Model_warden->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['InstitutePins'], true);
    			if(in_array($data['id'], $pinIds['pins'])) {
                    $pinIds['pins'] = $this->remove_array($pinIds['pins'], $data['id']);
    			}
    			else{
    			array_push($pinIds['pins'], $data['id']);
    		    }
    			$final_result['InstitutePins'] = json_encode($pinIds, true);
    			$this->Model_warden->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'InstitutePins'=>$final_result['InstitutePins']));
    			break;
    		case '4':
    			$this->load->model('Model_staff');
    			$user = $this->Model_staff->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['InstitutePins'], true);
    			if(in_array($data['id'], $pinIds['pins'])) {
                    $pinIds['pins'] = $this->remove_array($pinIds['pins'], $data['id']);
    			}
    			else{
    			array_push($pinIds['pins'], $data['id']);
    		    }
    			$final_result['InstitutePins'] = json_encode($pinIds, true);
    			$this->Model_staff->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'InstitutePins'=>$final_result['InstitutePins']));
    			break;		
    		default:
    			$this->response(array('success'=>false,'Message'=>'no ID exists with this'));
    			
    			break;
    	}
    	}else{
    		$this->response(array('success'=>false,'Message'=>'No such complaint exists with given ID or Not your Complaint'));
    	}

    }


	function comment_get(){
		$data = $this->get();
		$this->load->model('Model_comments');
		$this->load->model('Model_institute');
		$row = $this->Model_institute->get_by(array('id'=> $data['id']));
		if (isset($row['id'])) {
			$user = $this->session->all_userdata();
			$each_comm['id'] = null;
			$each_comm['UserID'] = $user['user_info']['info']['UserID'];
			$each_comm['Content'] = $data['content'];
			$each_comm['CreatedTime'] = date('Y-m-d H:i:s');
			$success = $this->Model_comments->insert($each_comm, false);
			if ($success == false) {
				$this->response(array('success'=> true, 'Message'=> 'Unable to insert comment'));
			}else{
				$ids_array = json_decode($row['CommentIDs'], true);
				array_push($ids_array['ids'], $success);
				$ret = $this->Model_institute->update($row['id'], array('CommentIDs'=> json_encode($ids_array),'UpdatedTime'=>date('Y-m-d H:i:s')), false);
				if ($ret == false) {
					$this->response(array('success'=> true, 'Message'=> 'Unable to update.'));
				}else{
					$this->response(array('success'=> true, 'Message'=> ''));
				}
			}
		}else{
			$this->response(array('success'=> true, 'Message'=> 'Id not present'));
		}
	}
}

?>