<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';
require APPPATH.'/controllers/composer/vendor/autoload.php';


use OpenTok\OpenTok;	
use OpenTok\MediaMode;
use OpenTok\ArchiveMode;
use OpenTok\Role;
use OpenTok\OutputMode;

class Test extends REST_Controller{

	function __construct() {
		parent::__construct();
	}

	function keys_get(){
		$apiKey = '45610682';
		$apiSecret = '001b0a40097dabb56c9d23c3e2647954bdd36849';
		$opentok = new OpenTok($apiKey, $apiSecret);

		

		// Create a session that attempts to use peer-to-peer streaming:
		// $session = $opentok->createSession();

		// A session that uses the OpenTok Media Router, which is required for archiving:
		// $session = $opentok->createSession(array('mediaMode' => MediaMode::ROUTED));

		// A session with a location hint:
		// $session = $opentok->createSession(array( 'location' => '12.34.56.78' ));

		// An automatically archived session:
		$sessionOptions = array(
		    'mediaMode' => MediaMode::ROUTED
		);
		$session = $opentok->createSession($sessionOptions);


		// Store this sessionId in the database for later use
		$sessionId = $session->getSessionId();

		

		// Generate a Token from just a sessionId (fetched from a database)
		// $token = $opentok->generateToken($sessionId);

		// Generate a Token by calling the method on the Session (returned from createSession)
		// $token = $session->generateToken();

		// Set some options in a token
		$token = $session->generateToken(array(
		    'role'       => Role::PUBLISHER,
		    'expireTime' => time()+(7 * 24 * 60 * 60)
		));

		// $archive = $opentok->startArchive($sessionId);

		$this->response(array('sessionId'=>$sessionId, 'token'=>$token, 'archiveId'=>0));//$archive->$archiveId));
	}


}

?>