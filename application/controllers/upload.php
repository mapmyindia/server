<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';


class Upload extends REST_Controller{

	function __construct()
	{
		parent::__construct();
	}

	function image_post()
	{
		// Path to move uploaded files
		$target_path = "./uploads/";
		 
		// array for final json respone
		$response = array();
		 
		// getting server ip address
		$server_ip = gethostbyname(gethostname());
		 
		 
		if (isset($_FILES['image']['name'])) {

			/*if ($_FILES["image"]["size"] > 500000) {
			    $response['constraint'] = "Size too large";
			}else{
				$response['constraint'] = "Size ok";
			}*/

			ini_set('upload_max_filesize', '10M');
			ini_set('post_max_size', '10M');
			ini_set('max_input_time', 300);
			ini_set('max_execution_time', 300);

		    $target_path = $target_path . basename($_FILES['image']['name']);
		 
		    try {
		        // Throws exception incase file is not being moved
		        if (!move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {
		            // make error flag true
		            $response['error'] = true;
		            $response['message'] = 'Could not move the file!';
		        }
		 
		        // File successfully uploaded
		        $response['message'] = 'File uploaded successfully!';
		        $response['error'] = false;
		    } catch (Exception $e) {
		        // Exception occurred. Make error flag true
		        $response['error'] = true;
		        $response['message'] = $e->getMessage();
		    }
		} else {
		    // File parameter is missing
		    $response['error'] = true;
		    $response['message'] = 'Not received any file!';
		}
		// Echo final json response to client
		$this->response($response);
	}
}

?>