<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Users extends REST_Controller{

	function __construct() {
		parent::__construct();
	}

	function user_get(){
		$data = $this->session->all_userdata();
		if (isset($data['user_info'])) {
			$data['user_info']['info']['Password'] = null;
			$this->response(array('user'=>$data['user_info'], 'success'=> true));
		}else{
			$this->response(array('user'=>null, 'success'=> false));
		}
	}
	function getuser_get(){
		$data = $this->get();
		$this->load->helper('find_user');
		$find_user=findUser(array('UserID'=>$data['UserId']),$this);
		if(isset($find_user['info']['id'])){
			$find_user['info']['Password']=null;
			$this->response(array('user'=>$find_user,'success'=>true));
		}else{
			$this->response(array('user'=>null,'success'=>true));
		}
	}


   function getphoto_post(){
   	$sess_user=$this->session->all_userdata();
   	$result = $sess_user['user_info'];
   	switch ($result['type']) {
   		case '1':
   			$this->load->model('Model_students');
   			$row = $this->Model_students->get_by(array('UserID'=>$result['info']['UserID']));
   			$this->response(array('success'=>true,'Message'=>$row['ImageFilePath']));
   			break;
   		case '2':
   			$this->load->model('Model_faculty');
   			$row = $this->Model_faculty->get_by(array('UserID'=>$result['info']['UserID']));
   			$this->response(array('success'=>true,'Message'=>$row['ImageFilePath']));
   			break;
   		case '3':
   			$this->load->model('Model_warden');
   			$row = $this->Model_warden->get_by(array('UserID'=>$result['info']['UserID']));
   			$this->response(array('success'=>true,'Message'=>$row['ImageFilePath']));
   			break;
   		case '4':
   			$this->load->model('Model_staff');
   			$row = $this->Model_staff->get_by(array('UserID'=>$result['info']['UserID']));
   			$this->response(array('success'=>true,'Message'=>$row['ImageFilePath']));
   			break;
   		
   		default:
   			$this->response(array('success'=>false,'Message'=>'Not recognised User'));
   			break;
   	}
   }

	function upload_post(){
		$data=$this->post();
		$sess_user=$this->session->all_userdata();
		$result=$sess_user['user_info'];
		if(isset($data['image'])){
			$result['info']['ImageFilePath']=$data['image'];
		}
		else{
			$this->response(array('success'=>false,'Message'=>'Select a Photo'));
			return;
		}
        switch ($sess_user['user_info']['type']) {
        	case cons_student:
          	  	       $this->load->model('Model_students');
          	  		   $this->Model_students->update($sess_user['user_info']['info']['id'],$result['info'],false);
          	  		break;
          	  	case cons_faculty:
          	  		   $this->load->model('Model_faculty');
          	  		   $this->Model_faculty->update($sess_user['user_info']['info']['id'],$result['info'],false);
          	  		break;
          	  	case cons_warden:
          	  		   $this->load->model('Model_warden');
          	  		   $this->Model_warden->update($sess_user['user_info']['info']['id'],$result['info'],false);
          	  		break;
          	  	case cons_staff:
          	  		   $this->load->model('Model_staff');
          	  		   $this->Model_staff->update($sess_user['user_info']['info']['id'],$result['info'],false);
          	  		break;			
          	  	
          	  	default:
          	  		$this->response(array('success'=>false,'message'=>'Not Updated'));
          	  		return;
          	  		break;
        }
        $this->response(array('success'=>true,'message'=>'Photo uploaded'));
	}

	function updateProfile_get(){
        $data = $this->get();
        $sess_user = $this->session->all_userdata();
        if($sess_user['user_info']['info']['Password']==$data['oldPassword']){
        	 $final_user=array();
          	  $result = $sess_user['user_info'];
              if(isset($data['newPassword'])){
              		$result['info']['Password'] = $data['newPassword'];
              }
              if(isset($data['FirstName'])){
              		$result['info']['FirstName'] = $data['FirstName'];
          	  }
          	  if(isset($data['LastName'])){
          	  	    $result['info']['LastName'] = $data['LastName'];
          	  }
          	switch ($sess_user['user_info']['type']) {
          	  	case cons_student:
          	  	       $this->load->model('Model_students');
          	  		   $this->Model_students->update($sess_user['user_info']['info']['id'],$result['info'],false);
          	  		break;
          	  	case cons_faculty:
          	  		   $this->load->model('Model_faculty');
          	  		   $this->Model_faculty->update($sess_user['user_info']['info']['id'],$result['info'],false);
          	  		break;
          	  	case cons_warden:
          	  		   $this->load->model('Model_warden');
          	  		   $this->Model_warden->update($sess_user['user_info']['info']['id'],$result['info'],false);
          	  		break;
          	  	case cons_staff:
          	  		   $this->load->model('Model_staff');
          	  		   $this->Model_staff->update($sess_user['user_info']['info']['id'],$result['info'],false);
          	  		break;			
          	  	
          	  	default:
          	  		$this->response(array('success'=>false,'message'=>'Not Updated'));
          	  		return;
          	  		break;
          	  }
          	  $this->session->set_userdata('user_info', $result);
          	  $this->response(array('success'=>true,'message'=>''));  
        }
        else{
        	$this->response(array('success'=>false,'message'=>'Incorrect Password'));
        }
	}

	function addDb($data){
        $this->load->helper('find_user');
        $find_user=findUser(array('UserID'=>$data['UserId']),$this);
        if(!isset($find_user['info']['id'])){
		$row['id'] = null;
		$row['UserID'] = $data['UserId'];
		$row['Password'] = $data['Password'];
		$row['FirstName'] = $data['FirstName'];
		$row['LastName'] = $data['LastName'];
		$row['Special'] = normal;
		$row['ImageFilePath'] = '';
		switch ($data['type']) {
			case cons_student:
				$row['HostelName'] = $data['HostelName'];
				$row['RoomNo'] = $data['RoomNo'];
				$row['IndividualPins'] = json_encode(array('pins'=>[]));
				$row['HostelPins'] = json_encode(array('pins'=>[]));
				$row['InstitutePins'] = json_encode(array('pins'=>[]));
				$this->load->model('Model_students');
				if($this->Model_students->insert($row, false) != false){
					$this->response(array('success'=> true));
				}else{
					$this->response(array('success'=> false));
				}
				break;

			case cons_faculty:
				$row['IndividualPins'] = json_encode(array('pins'=>[]));
				$row['InstitutePins'] = json_encode(array('pins'=>[]));
				$this->load->model('Model_faculty');
				if($this->Model_faculty->insert($row, false) != false){
					$this->response(array('success'=> true));
				}else{
					$this->response(array('success'=> false));
				}
				break;

			case cons_warden:
				$row['HostelName'] = $data['HostelName'];
				$row['IndividualPins'] = json_encode(array('pins'=>[]));
				$row['HostelPins'] = json_encode(array('pins'=>[]));
				$row['InstitutePins'] = json_encode(array('pins'=>[]));
				$this->load->model('Model_warden');
				if($this->Model_warden->insert($row, false) != false){
					$this->response(array('success'=> true));
				}else{
					$this->response(array('success'=> false));
				}
				break;

			case cons_staff:
				$row['Type'] = $data['Type'];
				$row['IndividualPins'] = json_encode(array('pins'=>[]));
				$row['institutepins'] = json_encode(array('pins'=>[]));
				$row['complaints'] = json_encode(array('ids'=>[]));
				$row['numcomplaints'] = zero;
				$this->load->model('Model_staff');
				if($this->Model_staff->insert($row, false) != false){
					$this->response(array('success'=> true));
				}else{
					$this->response(array('success'=> false));
				}
				break;
			
			default:
				$this->response(array('success'=> false));
				break;
		}}else{
			$this->response(array('success'=>false,'Message'=>'Duplicate UserId'));
		}
	}

	function add_get(){

		$this->addDb($this->get());		
	}

	function update_get(){
 		$data = $this->get();
		$this->load->helper('find_user');
		$user = $this->session->all_userdata();
		if($user['user_info']['info']['Special']==special){
			$find_user=findUser(array('UserID'=>$data['UserId']),$this);
            if($find_user['info']['Special']==special){
            	$this->response(array('success'=>false,'message'=>'You are not allowed to update special user'));
            	return;
            }
            $update = array();
            switch ($find_user['type']) {
            	case cons_student:
            		if(isset($data['Password'])){
            			$update['Password']=$data['Password'];
            		}
            		if(isset($data['HostelName'])){
		        		$update['HostelName']=$data['HostelName'];
		        	}
		        	if(isset($data['RoomNo'])){
		        		$update['RoomNo']=$data['RoomNo'];
		        	}
		        	$this->load->model('Model_students');
		        	$this->Model_students->update($find_user['info']['id'],$update,false);
		        	$this->response(array('success'=>true,'message'=>'Info Updated'));
            		break;
            	case cons_warden:
            	   if(isset($data['Password'])){
            			$update['Password']=$data['Password'];
            		}
            		if(isset($data['HostelName'])){
		        		$update['HostelName']=$data['HostelName'];
		        	}
		        	$this->load->model('Model_warden');
		        	$this->Model_warden->update($find_user['info']['id'],$update,false);
		        	$this->response(array('success'=>true,'message'=>'Info Updated'));
            		break;  
            	case cons_faculty:
            	    if(isset($data['Password'])){
            			$update['Password']=$data['Password'];
			        	$this->load->model('Model_faculty');
			        	$this->Model_faculty->update($find_user['info']['id'],$update,false);
			        	$this->response(array('success'=>true,'message'=>'Info Updated'));
            		}else{
            			$this->response(array('success'=>false,'message'=>'Required Password'));
            		}
            		break;
            	case cons_staff:
            	    if(isset($data['Password'])){
            			$update['Password']=$data['Password'];
			        	$this->load->model('Model_staff');
			        	$this->Model_staff->update($find_user['info']['id'],$update,false);
			        	$this->response(array('success'=>true,'message'=>'Info Updated'));
            		}else{
            			$this->response(array('success'=>false,'message'=>'Required Password'));
            		}
            		break; 			
            	
            	default:
            		$this->response(array('success'=>false,'message'=>'Not Updated'));
            		break;
            }
		}
		else{
			$this->response(array('success'=>false,'message'=>'You are not a special user'));
		}
		
	}

	function remove_get(){

		$UserId = $this->get('UserId');
		$this->load->helper('find_user');
		if (deleteUser($UserId, $this)) {
			$this->response(array('success'=> true));
		}else{
			$this->response(array('success'=> false));
		}
	}
}

?>