<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Users extends REST_Controller{

	function __construct() {
		parent::__construct();
	}

	/**
	*get current logged in user info
	*/
	function user_get(){
		//array is only a copy
		$data = $this->session->all_userdata();
		if (isset($data['user_info'])) {
			//do not send password
			$data['user_info']['password'] = null;
			$this->response(array('user'=>$data['user_info'], 'success'=> true));
		}else{
			//user session expired
			$this->response(array('user'=>null, 'success'=> false));
		}
	}

	/**
	*login by the user
	*set user session
	*/
	function login_post(){
		$data = $this->post();
		$this->load->helper('find_users');
		$this->load->model('Model_users');
		//find if user with given user name present
		$user = findUser(array('username'=>$data['username']), $this->Model_users);
		if ($user == false) {
			$this->response(array('success'=>false, 'message'=>'Invalid User Name/Unregistered'));
		}else if ($user['password'] == $data['password']) {
			//check if there is password match
			//set user session
			$this->session->set_userdata('user_info', $user);
			$this->response(array('success'=>true, 'message'=> ''));
		}else{
			//send message for password mismatch
			$this->response(array('success'=>false, 'message'=>'Password mismatch'));
		}
	}

	/**
	*register the user with the server
	*/
	function add_post(){

		$data = $this->post();
		$this->load->model('Model_users');
		$this->load->helper('find_users');
		//check if username already exists
		if (findUser(array('username'=>$data['username']), $this->Model_users)!=false) {
			$this->response(array('success'=>false, 'message'=>'username already present'));
		}else{
			$row = $data;
			$row['id'] = null;
			if($this->Model_users->insert($row, false)!=false){
				$this->response(array('success'=>true, 'message'=>''));
			}else{
				$this->response(array('success'=>false, 'message'=>'Something went wrong:P'));
			}
		}	
	}

	/**
	*destroys user session
	*/
	function logout_get(){
		$this->session->sess_destroy();
		$this->response(array('success' => true));
	}
}

?>