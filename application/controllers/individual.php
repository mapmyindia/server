<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Individual extends REST_Controller{

	function __construct() {
		parent::__construct();
	}

	function types_get(){
		$this->load->model('Model_staff');
		$all = $this->Model_staff->get_all();
		$types = array();
		foreach ($all as $row) {
			if (!in_array($row['Type'], $types)) {
				array_push($types, $row['Type']);
			}
		}
		$this->response(array('types'=>$types));
	}

	function complain_get(){
		$data = $this->get();
		$this->load->model('Model_staff');
		$this->load->model('Model_individual');
		$sess_user = $this->session->all_userdata();
		if (isset($sess_user['user_info'])) {
			$UserId = $sess_user['user_info']['info']['UserID'];
			$people = $this->Model_staff->get_many_by(array('Type'=> $data['type']));
			$num_works = false;
			$receiver = null;
			foreach ($people as $row) {
				$len = $row['numcomplaints'];
				if ($num_works == false) {
					$num_works = $len;
					$receiver = $row;
				}elseif ($len < $num_works) {
					$num_works = $len;
					$receiver = $row;
				}
			}
			$comp['id'] = null;
			$comp['SenderID'] = $UserId;
			if ($receiver == null) {
				$this->response(array('success'=>false, 'Message'=> 'No reciever available.'));
				return;
			}else{
				$comp['ReceiverID'] = $receiver['UserID'];				
			}
			$comp['Resolved'] = unresolved;
			$comp['Title'] = $data['Title'];
			$comp['Content'] = $data['Content'];
			$comp['CreatedTime'] = date('Y-m-d H:i:s');
			$comp['UpdatedTime'] = $comp['CreatedTime'];
			$comp['CommentIDs'] = json_encode(array('ids'=>[]));
			$result = $this->Model_individual->insert($comp, false);
			if ($result == false) {
				$this->response(array('success'=>false, 'Message'=> 'Validation Error'));
			}else{
				$id_array = json_decode($receiver['complaints'], true);
				array_push($id_array['ids'], $result);
				$final_update['complaints'] = json_encode($id_array);
				$final_update['numcomplaints'] = count($id_array['ids']);
				$after_update = $this->Model_staff->update($receiver['id'], $final_update, false);
				if ($after_update == false) {
					$this->response(array('success'=>'false', 'Message'=> 'Error updating staff'));
				}else{
					$this->response(array('success'=>true, 'Message' => ''));
				}
			}
		}else{
			$this->response(array('success'=>false, 'Message' => 'User session expired.'));
		}
	}

	function getcomplaint_get(){
		$data = $this->get();
		$this->load->model('Model_individual');
		$row = $this->Model_individual->get_by(array('id'=> $data['id']));
		$comments = [];
		if (isset($row['CommentIDs'])) {
			$this->load->model('Model_comments');
			$comment_ids = json_decode($row['CommentIDs'], true);
			$this->load->helper('find_user');
			$var = findUser(array('UserID'=> $row['ReceiverID']), $this);
			if(isset($var['info'])){
			$row['receiver'] = findUser(array('UserID'=> $row['ReceiverID']), $this)['info'];
			$row['receiver']['Password'] = null;
		    }
		    else{
		    	$row['receiver']['FirstName']='Unknown';
		    	$row['receiver']['LastName']='user';
		    	$row['receiver']['Type']='zero';
		    }
			foreach ($comment_ids['ids'] as $id) {
				$each_comp = $this->Model_comments->get_by(array('id'=> $id));
				if (isset($each_comp['id'])) {
					$final_comp['UserId'] = $each_comp['UserID'];
					$find_user = findUser(array('UserID'=> $each_comp['UserID']), $this);
					if (isset($find_user['info'])) {
						$final_comp['FirstName'] = $find_user['info']['FirstName'];
						$final_comp['LastName'] = $find_user['info']['LastName'];
					}
					else{
						$final_comp['FirstName'] = 'Unknown';
						$final_comp['LastName']  = 'User';
					}
					$final_comp['Content'] = $each_comp['Content'];
					$final_comp['CreatedTime'] = $each_comp['CreatedTime'];
					array_push($comments, $final_comp);
					}
			}
			$row['CommentIDs'] = null;
			$row['comments'] = $comments;
			$this->response(array('success'=>true, 'Message'=>'', 'complaint'=> $row));
		}else{
			$this->response(array('success'=>false, 'Message'=>'User ID not found.', 'complaint'=> ''));
		}
	}
    function pin_get(){
    	$data = $this->get();
    	$this->load->helper('find_user');
    	$this->load->model('Model_individual');
    	$row = $this->Model_individual->get_by(array('id'=>$data['id']));
    	$sess_user=$this->session->all_userdata();
    	if(isset($row['id']) && ($row['SenderID']==$sess_user['user_info']['info']['UserID'] ||$row['ReceiverID']==$sess_user['user_info']['info']['UserID']) ){
    	
    	switch ($sess_user['user_info']['type']) {
    		case '1':
    			$this->load->model('Model_students');
    			$user = $this->Model_students->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['IndividualPins'], true);
    			array_push($pinIds['pins'], $data['id']);
    			$final_result['IndividualPins'] = json_encode($pinIds, true);
    			$this->Model_students->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'IndividualPins'=>$final_result['IndividualPins']));
    			break;
    		case '2':
    			$this->load->model('Model_faculty');
    			$user = $this->Model_faculty->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['IndividualPins'], true);
    			array_push($pinIds['pins'], $data['id']);
    			$final_result['IndividualPins'] = json_encode($pinIds, true);
    			$this->Model_faculty->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'IndividualPins'=>$final_result['IndividualPins']));
    			break;
    		case '3':
    			$this->load->model('Model_warden');
    			$user = $this->Model_warden->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['IndividualPins'], true);
    			array_push($pinIds['pins'], $data['id']);
    			$final_result['IndividualPins'] = json_encode($pinIds, true);
    			$this->Model_warden->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'IndividualPins'=>$final_result['IndividualPins']));
    			break;
    		case '4':
    			$this->load->model('Model_staff');
    			$user = $this->Model_staff->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['IndividualPins'], true);
    			array_push($pinIds['pins'], $data['id']);
    			$final_result['IndividualPins'] = json_encode($pinIds, true);
    			$this->Model_staff->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'IndividualPins'=>$final_result['IndividualPins']));
    			break;		
    		default:
    			$this->response(array('success'=>false,'Message'=>'no ID exists with this'));
    			
    			break;
    	}
    	}else{
    		$this->response(array('success'=>false,'Message'=>'No such complaint exists with given ID or Not your Complaint'));
    	}

    }

function removearray($array, $data){
		$result = array();
		foreach ($array as $value) {
			if($data!=$value){
				array_push($result,$value);
			}
		}
		return $result;
	}

function unpin_get(){
    	$data = $this->get();
    	$this->load->helper('find_user');
    	$this->load->model('Model_individual');
    	$row = $this->Model_individual->get_by(array('id'=>$data['id']));
    	$sess_user=$this->session->all_userdata();
    	if(isset($row['id']) && ($row['SenderID']==$sess_user['user_info']['info']['UserID'] ||$row['ReceiverID']==$sess_user['user_info']['info']['UserID']) ){
    	
    	switch ($sess_user['user_info']['type']) {
    		case '1':
    			$this->load->model('Model_students');
    			$user = $this->Model_students->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['IndividualPins'], true);
    			if(in_array($data['id'], $pinIds['pins'])) {
                    $pinIds['pins'] = $this->removearray($pinIds['pins'], $data['id']);
    			}
    			else{
    			array_push($pinIds['pins'], $data['id']);
    		    }
    			$final_result['IndividualPins'] = json_encode($pinIds, true);
    			$this->Model_students->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'IndividualPins'=>$final_result['IndividualPins']));
    			break;
    		case '2':
    			$this->load->model('Model_faculty');
    			$user = $this->Model_faculty->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['IndividualPins'], true);
    			if(in_array($data['id'], $pinIds['pins'])) {
                    $pinIds['pins'] = $this->removearray($pinIds['pins'], $data['id']);
    			}
    			else{
    			array_push($pinIds['pins'], $data['id']);
    		    }
    		    $final_result['IndividualPins'] = json_encode($pinIds, true);
    			$this->Model_faculty->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'IndividualPins'=>$final_result['IndividualPins']));
    			break;
    		case '3':
    			$this->load->model('Model_warden');
    			$user = $this->Model_warden->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['IndividualPins'], true);
    			if(in_array($data['id'], $pinIds['pins'])) {
                    $pinIds['pins'] = $this->removearray($pinIds['pins'], $data['id']);
    			}
    			else{
    			array_push($pinIds['pins'], $data['id']);
    		    }
    		    $final_result['IndividualPins'] = json_encode($pinIds, true);
    			$this->Model_warden->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'IndividualPins'=>$final_result['IndividualPins']));
    			break;
    		case '4':
    			$this->load->model('Model_staff');
    			$user = $this->Model_staff->get_by(array('UserID'=>$sess_user['user_info']['info']['UserID']));
    			$pinIds = json_decode($user['IndividualPins'], true);
    			if(in_array($data['id'], $pinIds['pins'])) {
                    $pinIds['pins'] = $this->removearray($pinIds['pins'], $data['id']);
    			}
    			else{
    			array_push($pinIds['pins'], $data['id']);
    		    }
    		    $final_result['IndividualPins'] = json_encode($pinIds, true);
    			$this->Model_staff->update($sess_user['user_info']['info']['id'],$final_result,false);
    			$this->response(array('success'=>true,'IndividualPins'=>$final_result['IndividualPins']));
    			break;		
    		default:
    			$this->response(array('success'=>false,'Message'=>'no ID exists with this'));
    			
    			break;
    	}
    	}else{
    		$this->response(array('success'=>false,'Message'=>'No such complaint exists with given ID or Not your Complaint'));
    	}

    }
	function getwork_get(){
		$data = $this->get();
		$this->load->model('Model_individual');
		$row = $this->Model_individual->get_by(array('id'=> $data['id']));
		$comments = [];
		if (isset($row['CommentIDs'])) {
			$this->load->model('Model_comments');
			$comment_ids = json_decode($row['CommentIDs'], true);
			$this->load->helper('find_user');
			if(isset(findUser(array('UserID'=>$row['SenderID']),$this)['info'])){
			$row['sender'] = findUser(array('UserID'=> $row['SenderID']), $this)['info'];
			$row['sender']['Password'] = null;}
			else{
				$row['SenderID']='removed';
				$row['sender']['FirstName']='Unknown';
				$row['sender']['LastName']='User';
			}
			foreach ($comment_ids['ids'] as $id) {
				$each_comp = $this->Model_comments->get_by(array('id'=> $id));
				if (isset($each_comp['id'])) {
					$final_comp['UserId'] = $each_comp['UserID'];
					$find_user = findUser(array('UserID'=> $each_comp['UserID']), $this);
					if (isset($find_user['info'])) {
						$final_comp['FirstName'] = $find_user['info']['FirstName'];
						$final_comp['LastName'] = $find_user['info']['LastName'];
					}
					else{
						$final_comp['UserId']='removed';
						$final_comp['FirstName'] = 'Unknown';
						$final_comp['LastName']  = 'User';
					}
					$final_comp['Content'] = $each_comp['Content'];
					$final_comp['CreatedTime'] = $each_comp['CreatedTime'];
					array_push($comments, $final_comp);
				}
			}
			$row['CommentIDs'] = null;
			$row['comments'] = $comments;
			$this->response(array('success'=>true, 'Message'=>'', 'complaint'=> $row));
		}else{
			$this->response(array('success'=>false, 'Message'=>'User ID not found.', 'complaint'=> ''));
		}
	}
    function resolve_get(){
    	$data = $this->get();
    	$user = $this->session->all_userdata();
    	$this->load->model('Model_individual');
    	$row = $this->Model_individual->get_by(array('id'=>$data['id']));
    	if($row['Resolved']==0){
    	if(isset($row['id'])) {
    		if($row['SenderID']==$user['user_info']['info']['UserID']){
    			$final_comp['Resolved']=resolved;
    			$final_comp['UpdatedTime']= date('Y-m-d H:i:s');
    			$this->Model_individual->update($data['id'],$final_comp,false);
    			$this->response(array('success'=>true,'message'=>''));
    			return;
    		} else{
               $this->response(array('success'=>false,'message'=>'You are not allowed to resolve it.'));
    		}
    	}else{
    		$this->response(array('succes'=>false,'message'=>'Complaint ID does not exist.'));
    	}}
    	else{
    		$this->response(array('success'=>false,'Message'=>'Already Resolved'));
    	}
    } 



	function comment_get(){
		$data = $this->get();
		$this->load->model('Model_comments');
		$this->load->model('Model_individual');
		$row = $this->Model_individual->get_by(array('id'=> $data['id']));
		if (isset($row['id'])) {
			$user = $this->session->all_userdata();
			$each_comm['id'] = null;
			$each_comm['UserID'] = $user['user_info']['info']['UserID'];
			$each_comm['Content'] = $data['content'];
			$each_comm['CreatedTime'] = date('Y-m-d H:i:s');
			$success = $this->Model_comments->insert($each_comm, false);
			if ($success == false) {
				$this->response(array('success'=> true, 'Message'=> 'Unable to insert comment'));
			}else{
				$ids_array = json_decode($row['CommentIDs'], true);
				array_push($ids_array['ids'], $success);
				$ret = $this->Model_individual->update($row['id'], array('CommentIDs'=> json_encode($ids_array),'UpdatedTime'=>date('Y-m-d H:i:s')), false);
				if ($ret == false) {
					$this->response(array('success'=> true, 'Message'=> 'Unable to update.'));
				}else{
					$this->response(array('success'=> true, 'Message'=> ''));
				}
			}
		}else{
			$this->response(array('success'=> true, 'Message'=> 'Id not present'));
		}
	}
}


?>